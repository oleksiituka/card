<?php

namespace core;

Interface IResponse
{
    public function send($data);
}