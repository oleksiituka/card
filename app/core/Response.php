<?php

namespace core;

class Response implements IResponse
{
    private function setHeaders()
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
    }

    public function send($data = [])
    {
        $this->setHeaders();
        echo json_encode($data);
    }
}