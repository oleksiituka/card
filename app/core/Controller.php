<?php

namespace core;

class Controller
{
    protected $request;
    protected $response;

    public function __construct(ARequest $request, IResponse $response)
    {
        $this->request = $request;
        $this->response = $response;
    }
}