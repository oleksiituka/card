<?php

namespace core;

abstract class ARequest
{
    public $controller;
    public $action;
    public $params;

    abstract public function __construct();
}