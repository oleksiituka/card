<?php

namespace core;

interface IRouter
{
    public function process(ARequest $request, IResponse $response);
}