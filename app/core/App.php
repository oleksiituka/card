<?php

namespace core;

class App
{
    public $request;
    public $response;
    public $router;

    public function __construct(ARequest $request = null, IResponse $response = null, IRouter $router = null)
    {
        $this->request = $request ?? new Request();
        $this->response = $response ?? new Response();
        $this->router = $router ?? new Router();
    }

    public function run()
    {
        $this->router->process($this->request, $this->response);
    }
}