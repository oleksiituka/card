<?php

namespace core;

class Router implements IRouter
{
    public function process(ARequest $request, IResponse $response)
    {
        if($fileExists = file_exists(sprintf('%s/%s.php', realpath(__DIR__ . '/../controllers/'), $request->controller))) {
            $className = 'controllers\\' . $request->controller;
            $controller = new $className($request, $response);

            if($methodExists = method_exists($controller, $request->action)) {
                $actionName = $request->action;
                $controller->$actionName();
            }
        }

        if(!($fileExists && $methodExists)) {
            header('HTTP/1.1 400 Bad Request');
        }
    }
}