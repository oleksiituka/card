<?php

namespace core;

class Request extends ARequest
{
    public function __construct()
    {
        $this->parseUrl();
    }

    private function parseUrl()
    {
        $path = explode('/', trim($_SERVER['REQUEST_URI'],'/'));

        $this->controller = ucfirst(array_shift($path)) ?: 'Index';
        $this->action = array_shift($path) ?: 'index';

        if($_SERVER["CONTENT_TYPE"] == 'application/json') {
            $this->params = json_decode(file_get_contents('php://input'), true);
        } else {
            $this->params = array_merge($_GET, $_POST);
        }
    }
}