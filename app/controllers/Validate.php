<?php

namespace controllers;

use core\Controller as Controller;
use services\Validate as ValidateService;

class Validate extends Controller
{
    public function index()
    {
        $validateService = new ValidateService();
        $this->response->send($validateService->validate($this->request->params));
    }
}