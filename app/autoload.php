<?php

function autoLoader($className)
{
    require_once(__DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php');
}

spl_autoload_register('autoLoader');