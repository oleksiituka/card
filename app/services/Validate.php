<?php

namespace services;

class Validate
{
    public function validate($params)
    {
        return [
            'cardnumber' => $this->validateCardNumber((string)$params['cardnumber']),
            'cardholder' => $this->validateCardHolder((string)$params['cardholder'])
        ];
    }

    private function validateCardNumber($cardNumber)
    {
        $number = strrev(preg_replace('/[^\d]/', '', $cardNumber));
        $sum = 0;

        for($i = 0, $j = strlen($number); $i < $j; $i++) {
            if(($i % 2) == 0) {
                $val = $number[$i];
            } else {
                $val = $number[$i] * 2;
                if ($val > 9)  {
                    $val -= 9;
                }
            }
            $sum += $val;
        }

        return ($sum % 10) === 0 && $number !== '';
    }

    private function validateCardHolder($cardHolder)
    {
        return (bool)preg_match('/^[a-zA-Z\s]+$/', $cardHolder);
    }
}