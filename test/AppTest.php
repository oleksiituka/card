<?php

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testApp($data, $expected)
    {
        if($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'http://127.0.0.1/validate');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($curl);
            curl_close($curl);
        }

        $this->assertEquals($expected, $result);
    }

    public function dataProvider()
    {
        return [
            [
                json_encode(['cardnumber' => '1425 2547 2145 4555', 'cardholder' => 'Same User']),
                json_encode(['cardnumber' => false, 'cardholder' => true])
            ],
            [
                json_encode(['cardnumber' => '4111111111111111', 'cardholder' => 'User1']),
                json_encode(['cardnumber' => true, 'cardholder' => false])
            ],
            [
                json_encode(['cardnumber' => '', 'cardholder' => '']),
                json_encode(['cardnumber' => false, 'cardholder' => false])
            ],
            [
                json_encode(['cardnumber' => '3000 0000 0000 04', 'cardholder' => 'Same User.']),
                json_encode(['cardnumber' => true, 'cardholder' => false])
            ],
            [
                json_encode(['cardnumber' => '5500 0000 0000 0004', 'cardholder' => 'Senior Developer']),
                json_encode(['cardnumber' => true, 'cardholder' => true])
            ],
        ];
    }
}