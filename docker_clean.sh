#!/usr/bin/env bash
cd docker;
docker-compose down;
docker stop $(docker ps -a -q);
docker rm $(docker ps -a -q);
docker rmi -f $(docker images -q);
docker volume rm $(docker volume ls -f dangling=true -q);
cd ..;

