How to run test

1. Install PHP 7.3
2. Run local server:
    cd ./public && sudo /etc/init.d/apache2 stop && sudo php -S 127.0.0.1:80
3. Run command:
    php phpunit.phar ./test/AppTest.php