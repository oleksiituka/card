<?php

ini_set('display_errors', 1);

require_once(__DIR__ . '/../app/autoload.php');

$app = new \core\App();
$app->run();